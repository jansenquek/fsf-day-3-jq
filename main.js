//load express after you have done npm install --save express
var express = require ("express");
//Create an application
var app = express();

//create document root directory as public
// express.static = static files will come from this directory
app.use (express.static(__dirname + "/public"));

//Start our webserver on port 3000
//Adding the function is to check on console whether the code is working or not.
app.listen(3000, function () {
    console.info("My Webserver has started on port 3000");
    console.info("Document root is at " + __dirname + "/public");
    console.info("Copyright 2016")

}
);